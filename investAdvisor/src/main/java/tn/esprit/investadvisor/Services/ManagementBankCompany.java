package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyLocal;
import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyRemote;
import tn.esprit.investadvisor.entities.BankCompany;

/**
 * <p>
 * Session Bean implementation class <b>ManagementBankCompany</b> , it
 * encapsulates the services provided by EJB container for a  {@link BankCompany}.
 * <p>
 * 
 * @author SEIF BOUANANI
 */
@Stateless
@LocalBean
public class ManagementBankCompany implements IManagementBankCompanyRemote,
		IManagementBankCompanyLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementBankCompany() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds a {@link BankCompany} considered as new to the persistance
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database.
	 * <p>
	 */
	@Override
	public void addBankCompany(BankCompany bankCompany) {

		entityManager.persist(bankCompany);

	}

	/**
	 * <p>
	 * This service adds a {@link BankCompany} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database.
	 * <p>
	 */
	@Override
	public void updateBankCompany(BankCompany bankCompany) {
		entityManager.merge(bankCompany);
	}

	/**
	 * <p>
	 * This service marks a {@link BankCompany} as being managed in the active
	 * peristence context as removed. Thus, delete doesn't remove the
	 * {@link BankCompany} from thedatabase. When the entity manager comes to flush it
	 * synchronizes with thedatabase. If the entity is in context and labeled as
	 * "removed", it willbe then removed form the database.
	 * <p>
	 */
	@Override
	public void deleteBankCompany(BankCompany bankCompany) {

		entityManager.remove(entityManager.merge(bankCompany));
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a {@link BankCompany} by
	 * Id.
	 * <p>
	 */
	@Override
	public BankCompany findBankCompanyById(Integer idBankCompany) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link BankCompany} from the database. As the reuslt fetching are more
	 * then one result , thus we're using "getResultList" and each result can be
	 * tuned via the object query
	 * <p>
	 */
	@Override
	public List<BankCompany> findAll() {
		Query query = entityManager.createQuery("select s from BankCompany s");
		return query.getResultList();
	}
}
