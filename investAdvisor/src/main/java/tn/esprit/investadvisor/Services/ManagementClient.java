package tn.esprit.investadvisor.Services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.Client.IManagementClientRemote;
import tn.esprit.investadvisor.entities.Admin;
import tn.esprit.investadvisor.entities.Client;

/**
 * <p>
 * Session Bean implementation class ManagementClient , it encapsulates the
 * services provided by the EJB container for a {@link Client} Entity.
 * <p>
 * 
 * @author SEIF BOUANANI
 */
@Stateless
public class ManagementClient implements IManagementClientRemote,
		IManagementClientLocal {
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementClient() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds a {@link Client} Entity considered as new to the
	 * peristence context.When the entity manager comes to flush it synchronizes
	 * with the database
	 * <p>
	 */
	@Override
	public void addClient(Client client) {
		entityManager.persist(client);
	}

	/**
	 * <p>
	 * This service adds a {@link Client} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database
	 * <p>
	 */
	@Override
	public void updateClient(Client client) {
		entityManager.merge(client);
	}

	/**
	 * <p>
	 * This service marks {@link Client}as being managed in the active
	 * peristence context as removed. Thus, delete doesn't remove the
	 * {@link Client} from the database. When the entity manager comes to flush
	 * it synchronizes with the database. If the entity is in context and
	 * labeled as "removed", it willbe then removed form the database.
	 * <p>
	 */
	@Override
	public void deleteClient(Client client) {
		entityManager.remove(entityManager.merge(client));
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Client} by Id.
	 */
	@Override
	public Client findClientById(int id) {
		Client client = null;
		client = entityManager.find(Client.class, id);
		return client;
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link Client} from the database.
	 * <p>
	 */
	@Override
	public List<Client> findAllClient() {
		Query query = entityManager.createQuery("select c from Client c");
		return query.getResultList();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Client} by Id.
	 * <p>
	 */
	@Override
	public boolean findClientByLoginBo(String login) {

		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.clientLogin =:login",
				Client.class);
		query.setParameter("login", login);
		
		return query.getResultList().size() > 0;
	}
	@Override
	public Client findClientByLogin(String login) {

		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.clientLogin =:login",
				Client.class);
		query.setParameter("login", login);
		
		return query.getSingleResult();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Client} by State. this method is used to descriminate the Non
	 * confirmed Client from Confirmed by the Admin
	 * <p>
	 */
	@Override
	public List<Client> findClientByState(Integer state) {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.valid =:valid", Client.class);
		query.setParameter("valid", state);
		return query.getResultList();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Client} by Date of registration. this Method is used by the
	 * <b>Admin</b> to filter registration by Date , and statistics
	 * <p>
	 */
	@Override
	public List<Client> findClientByRegistrationDate(Date date) {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.registrationDate =:date",
				Client.class);
		query.setParameter("date", date);
		return query.getResultList();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Client} by Last Connexion Date.
	 * <p>
	 */
	@Override
	public List<Client> findClientByLastConnectionDate(Date date) {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.lastConnectionDate =:date",
				Client.class);
		query.setParameter("date", date);
		return query.getResultList();
	}

	@Override
	public List<Client> findClientByLastConnectionDate() {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.lastConnectionDate is null",
				Client.class);
		
		return query.getResultList();
	}

	@Override
	public List<Client> findClientByLastConnectionDateNotNull() {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.lastConnectionDate is not null",
				Client.class);
	
		return query.getResultList();
	}

	@Override
	public Client findClientByEmail(String Email) {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.mail =:login",
				Client.class);
		query.setParameter("login", Email);
		
		return query.getSingleResult();
	}
	@Override
	public boolean findClientByEmailBo(String Email) {
		TypedQuery<Client> query = entityManager.createQuery(
				"select a from Client a where a.mail =:login",
				Client.class);
		query.setParameter("login", Email);
		
		return query.getResultList().size() >0 ;
	}

}
