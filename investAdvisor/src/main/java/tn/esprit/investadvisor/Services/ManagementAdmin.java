package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.admin.IManagementAdminLocal;
import tn.esprit.investadvisor.contracts.admin.IManagementAdminRemote;
import tn.esprit.investadvisor.entities.Admin;

/**
 * The Session Bean implementation class ManagementAdmin , it contains the
 * implementations of all the services provided by EJB container for an
 * {@link Admin} Entity .
 * 
 * @author SEIF BOUANANI
 * 
 *         <p>
 *
 */
@Stateless
public class ManagementAdmin implements IManagementAdminLocal,
		IManagementAdminRemote {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementAdmin() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds an {@link Admin} considered as new to the peristence
	 * context.When the entity manager comes to flush it synchronizes with the
	 * database
	 * <p>
	 */
	@Override
	public void addAdmin(Admin admin) {
		entityManager.persist(admin);
	}

	/**
	 * <p>
	 * This service adds an {@link Admin} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database.
	 * <p>
	 */
	@Override
	public void updateAdmin(Admin admin) {

		entityManager.merge(admin);

	}

	/**
	 * <p>
	 * This service marks an {@link Admin} being managed in the active
	 * peristence context as removed. Thus, delete doesn't remove the
	 * {@link Admin} from thedatabase. When the entity manager comes to flush it
	 * synchronizes with thedatabase. If the entity is in context and labeled as
	 * "removed", it willbe then removed form the database.
	 * <p>
	 */
	@Override
	public void deleteAdmin(Admin admin) {
		entityManager.remove(entityManager.merge(admin));

	}

	/**
	 * <p>
	 * This service fetchs by id a {@link Admin} from the database and puts it
	 * in the active peristence context.
	 * <p>
	 */
	@Override
	public Admin findAdminById(Integer idAdmin) {
		Admin admin = null;

		admin = entityManager.find(Admin.class, idAdmin);
		return admin;
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link Admin} from the database. As the reuslt fetching {@link Admin} are
	 * more then one result , thus we're using "getResultList" and each result
	 * can be tuned via the object query
	 * <p>
	 */
	@Override
	public List<Admin> findAllAdmin() {
		Query query = entityManager.createQuery("select a from Admin a");
		return query.getResultList();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch an
	 * {@link Admin} by Login. Below we are expecting a unique result. Thus
	 * we're using "getSingleResult" which returns the unique result if exists.
	 * <p>
	 * This method is often used in our authentication functionnality for the
	 * {@link Admin} .
	 * <p>
	 */
	@Override
	public Admin findAdminByLogin(String login) {

		TypedQuery<Admin> query = entityManager.createQuery(
				"select a from Admin a where a.login =:login", Admin.class);
		query.setParameter("login", login);
		return query.getSingleResult();

	}

}
