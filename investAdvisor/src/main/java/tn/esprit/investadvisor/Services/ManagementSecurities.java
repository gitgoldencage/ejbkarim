package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.Securities.IManagementSecuritiesLocal;
import tn.esprit.investadvisor.contracts.Securities.IManagementSecuritiesRemote;
import tn.esprit.investadvisor.entities.Bonds;
import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.Stock;

/**
 * Session Bean implementation class ManagementSecurities , it encapsulates all
 * the services provided by the EJB container for both {@link Bonds} and
 * {@link Stock} entities.
 * 
 * Since this Entity is abstract , subclassed by both Stocks and Bonds , the
 * parametre  {@link Securities} passed on all methods implemented in this class
 * will be specified as one of the two types of securities .
 * 
 * @author SEIF BOUANANI
 * */
@Stateless
@LocalBean
public class ManagementSecurities implements IManagementSecuritiesRemote,
		IManagementSecuritiesLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementSecurities() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link Securities} considered as new to the
	 * peristence context.
	 */
	@Override
	public void addSecurities(Securities securities) {

		entityManager.persist(securities);

	}

	/**
	 * This service adds a {@link Securities} to the peristence context.The
	 * entity manager rather stores it as managed or attached to the active
	 * persistence context.
	 */
	@Override
	public void updateSecurities(Securities securities) {
		entityManager.merge(securities);
	}

	/**
	 * This service marks a {@link Securities} Entity as being managed in the
	 * active peristence context as removed.
	 */
	@Override
	public void deleteSecurities(Securities securities) {

		entityManager.remove(entityManager.merge(securities));
	}

	/**
	 * his services uses JPQL to ask the entity manager to fetch a
	 * {@link Securities} by Id.
	 */
	@Override
	public Securities findSecuritiesById(Integer idSecurities) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link Securities} from the database.
	 */
	@Override
	public List<Securities> findAll() {
		Query query = entityManager.createQuery("select s from Securities s");
		return query.getResultList();
	}

}
