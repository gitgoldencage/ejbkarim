package tn.esprit.investadvisor.Services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsLocal;
import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsRemote;
import tn.esprit.investadvisor.entities.Claims;

/**
 * Session Bean implementation class ManagementClaims
 */
@Stateless
public class ManagementClaims implements ManagementClaimsLocal, ManagementClaimsRemote {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;
	
	@Override
	public void addClaims(Claims claim) {
		entityManager.persist(claim);
	}

	@Override
	public void updateClaims(Claims claims) {
		entityManager.merge(claims);
	}

	@Override
	public void deleteClaims(Claims claims) {
		entityManager.remove(claims);
	}

	@Override
	public Claims findClaimsById(Integer idClaims) {
		Claims claim = new Claims();
		claim=entityManager.find(Claims.class, idClaims);
		return claim;
	}

	@Override
	public List<Claims> findAllClaims() {
		Query query = entityManager.createQuery("select c from Claims c");
		return query.getResultList();
	}

	@Override
	public List<Claims> findClaimsByDate(Date date) {
		TypedQuery<Claims> query = entityManager.createQuery(
				"select c from Claims a where a.date_claim =:date",
				Claims.class);
		query.setParameter("date", date);
		return query.getResultList();
	}

	@Override
	public List<Claims> findclaimsByUserId(Integer userId) {
		TypedQuery<Claims> query = entityManager.createQuery(
				"select a from Claims a where a.client.id =:userId",
				Claims.class);
		query.setParameter("userId", userId);
		return query.getResultList();
	}
	
	

   
}
