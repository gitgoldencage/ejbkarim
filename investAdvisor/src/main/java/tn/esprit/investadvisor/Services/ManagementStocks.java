package tn.esprit.investadvisor.Services;

import java.awt.Container;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.Stock.IManagementStocksLocal;
import tn.esprit.investadvisor.contracts.Stock.IManagementStocksRemote;
import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.Stock;

/**
 * Session Bean implementation class ManagementStocks , it implements all the
 * services provided by the EJB container for a {@link Stock} Entity.
 * 
 * The {@link Stock} is a class that inherit all its attributes and methods from
 * {@link Securities} , thus the methods implemented in this class and
 * {@link ManagementSecurities} are similar.
 * 
 * @author SEIF BOUANANI
 */
@Stateless
public class ManagementStocks implements IManagementStocksRemote,
		IManagementStocksLocal {
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entitymanager;

	public ManagementStocks() {

	}

	@Override
	public void addStock(Stock stock) {
		entitymanager.persist(stock);
	}

	@Override
	public void updateStock(Stock stock) {
		entitymanager.merge(stock);
	}

	@Override
	public void deleteStock(Stock stock) {
		entitymanager.remove(entitymanager.merge(stock));
	}

	/**
	 * encore a terminer
	 */
	@Override
	public List<Stock> findAllStocks() {
		Query query = entitymanager.createQuery("select s from Stock");
		return query.getResultList();
	}

	@Override
	public Stock findStockById(int id) {
		Stock stock = new Stock();
		stock = entitymanager.find(Stock.class, id);
		return stock;

	}

}
