package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Company.IManagementCompanyLocal;
import tn.esprit.investadvisor.contracts.Company.IManagementCompanyRemote;
import tn.esprit.investadvisor.entities.Admin;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Company;
import tn.esprit.investadvisor.entities.Sector;

/**
 * Session Bean implementation class ManagementCompany , it encapsulates the
 * services provided by EJB container for a {@link Company} entity .
 * 
 * @author SEIF BOUANANI
 */
@Stateless
@LocalBean
public class ManagementCompany implements IManagementCompanyRemote,
		IManagementCompanyLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementCompany() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link Company} considered as new to the peristence
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database
	 */
	@Override
	public void addCompany(Company company) {
		entityManager.persist(company);

	}

	/**
	 * This service adds a {@link Company} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context.
	 */
	@Override
	public void updateCompany(Company company) {
		entityManager.merge(company);
	}

	/**
	 * This service marks a {@link Company} as being managed in the active
	 * peristence context as removed. Thus, delete doesn't remove the Entity
	 * from thedatabase. When the entity manager comes to flush it synchronizes
	 * with thedatabase. If the entity is in context and labeled as "removed",
	 * it willbe then removed form the database.
	 */
	@Override
	public void deleteCompany(Company company) {
		entityManager.remove(company);

	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Company} by Id.
	 */
	@Override
	public Company findCompanyById(int id) {
		Company client = null;
		client = entityManager.find(Company.class, id);
		return client;
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Company} of a specefic <b>sector</b>.
	 */
	@Override
	public List<Company> findCompanyBySector(Sector sector) {
		TypedQuery<Company> query = entityManager.createQuery(
				"select a from Company a where a.sector =:sector", Company.class);
		query.setParameter("sector", sector);
		return query.getResultList();
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * <b>Companies</b> from the database.
	 * <p>
	 */
	@Override
	public List<Company> findAllCompany() {
		Query query = entityManager.createQuery("select c from Company c");
		return query.getResultList();
	}

	@Override
	public Company findCompanyByName(String name) {
		TypedQuery<Company> query = entityManager.createQuery(
				"select a from Company a where a.name =:name",
				Company.class);
		query.setParameter("name", name);
		return query.getSingleResult();
	}
	

}
