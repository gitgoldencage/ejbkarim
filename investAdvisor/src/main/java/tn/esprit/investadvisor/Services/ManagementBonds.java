package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.Bonds.IManagementBondsLocal;
import tn.esprit.investadvisor.contracts.Bonds.IManagementBondsRemote;
import tn.esprit.investadvisor.entities.Bonds;

/**
 * Session Bean implementation class ManagementBonds , it encapsulates the
 * services provided by the EJB container for a {@link Bonds}.
 * <p>
 */
@Stateless
public class ManagementBonds implements IManagementBondsRemote,
		IManagementBondsLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementBonds() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds a {@link Bonds} considered as new to the peristence
	 * context.When the entity manager comes to flush it synchronizes with the
	 * database.
	 * <p>
	 */
	@Override
	public void addBonds(Bonds bonds) {

		entityManager.persist(bonds);

	}

	/**
	 * <p>
	 * This service adds a {@link Bonds} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context. When the entity manager comes to flush it synchronizes with the
	 * database
	 * <p>
	 */
	@Override
	public void updateBonds(Bonds bonds) {
		entityManager.merge(bonds);
	}

	/**
	 * <p>
	 * This service marks a {@link Bonds} as being managed in the active
	 * peristence context as removed. Thus, delete doesn't remove the
	 * {@link Bonds} from thedatabase. When the entity manager comes to flush it
	 * synchronizes with thedatabase. If the entity is in context and labeled as
	 * "removed", it will be then removed form the database.
	 * <p>
	 */
	@Override
	public void deleteBonds(Bonds bonds) {

		entityManager.remove(entityManager.merge(bonds));
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a {@link Bonds}
	 * by its Id.
	 * <p>
	 */
	@Override
	public Bonds findBondsById(Integer idBonds) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * <b>admin</b> from the database.
	 * <p>
	 */
	@Override
	public List<Bonds> findAll() {
		Query query = entityManager.createQuery("select s from Securities s");
		return query.getResultList();
	}
}
