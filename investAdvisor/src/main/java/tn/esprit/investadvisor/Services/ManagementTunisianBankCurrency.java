package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.TunisianBankCurrency.IManagementTunisianBankCurrencyLocal;
import tn.esprit.investadvisor.contracts.TunisianBankCurrency.IManagementTunisianBankCurrencyRemote;
import tn.esprit.investadvisor.entities.TunisanBankCurrency;

/**
 * Session Bean implementation class ManagementTunisianBankCurrency 
 */
@Stateless
@LocalBean
public class ManagementTunisianBankCurrency implements IManagementTunisianBankCurrencyRemote, IManagementTunisianBankCurrencyLocal {
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementTunisianBankCurrency() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addTunisanBankCurrency(TunisanBankCurrency tunisianBankCurrency) {
	
			entityManager.persist(tunisianBankCurrency);
			
	}

	@Override
	public void updateTunisanBankCurrency(TunisanBankCurrency tunisianBankCurrency) {
			entityManager.merge(tunisianBankCurrency);
	}

	@Override
	public void deleteTunisanBankCurrency(TunisanBankCurrency tunisianBankCurrency) {

			entityManager.remove(entityManager.merge(tunisianBankCurrency));
	}

	@Override
	public TunisanBankCurrency findTunisanBankCurrencyById(Integer idTunisianBankCurrency) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TunisanBankCurrency> findAll() {
		Query query = entityManager.createQuery("select s from Currency s");
		return query.getResultList();
	}
}
