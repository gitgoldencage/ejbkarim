package tn.esprit.investadvisor.Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.Operation.IManagementOperationLocal;
import tn.esprit.investadvisor.contracts.Operation.IManagementOperationRemote;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Operation;

/**
 * Session Bean implementation class ManagementOperation , it encapsulates all
 * the services provided by the EJB container for an <b>Operation</b> Entity.
 *
 *@author SEIF BOUANANI
 */
@Stateless
@LocalBean
public class ManagementOperation implements IManagementOperationRemote,
		IManagementOperationLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementOperation() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds an <b>Operation</b> considered as new to the peristence
	 * context.When the entity manager comes to flush it synchronizes with the
	 * database
	 */
	@Override
	public void addOperation(Operation operation) {

		entityManager.persist(operation);

	}

	/**
	 * This service adds an {@link Operation} to the peristence context.The
	 * entity manager rather stores it as managed or attached to the active
	 * persistence context. When the entity manager comes to flush it
	 * synchronizes with the database
	 */
	@Override
	public void updateOperation(Operation operation) {
		entityManager.merge(operation);
	}

	/**
	 * This service marks an {@link Operation} as being managed in the active
	 * peristence context as removed.
	 */
	@Override
	public void deleteOperation(Operation operation) {

		entityManager.remove(entityManager.merge(operation));
	}
	
	
	public  List<Operation>findOperationByClient(Client client){
		List<Operation> operations=findAll();
		List<Operation> operationsClient= new ArrayList<Operation>();
		for(Operation o:operations){
			if(o.getOperationPk().getWalletId()==client.getWallet().getId())
				operationsClient.add(o);
		}
		return operationsClient;
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Operation} by Id passed in parametre.
	 */
	@Override
	public Operation findOperationById(Integer idOperation) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link Operation}s from the database.
	 */
	@Override
	public List<Operation> findAll() {
		Query query = entityManager.createQuery("select s from Operation s");
		return query.getResultList();
	}

}
