package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.StockHistorizationSecurities.IManagementStockHistorizationSecuritiesLocal;
import tn.esprit.investadvisor.contracts.StockHistorizationSecurities.IManagementStockHistorizationSecuritiesRemote;
import tn.esprit.investadvisor.entities.StockHistorizationSecurities;

/**
 * Session Bean implementation class ManagementStockHistorizationSecurities , it
 * contains the implementations of the services provided by the EJB container
 * for a {@link StockHistorizationSecurities} Entity.
 */
@Stateless
@LocalBean
public class ManagementStockHistorizationSecurities implements
		IManagementStockHistorizationSecuritiesRemote,
		IManagementStockHistorizationSecuritiesLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementStockHistorizationSecurities() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addStockHistorizationSecurities(
			StockHistorizationSecurities stockHistorizationSecurities) {

		entityManager.persist(stockHistorizationSecurities);

	}

	@Override
	public void updateStockHistorizationSecurities(
			StockHistorizationSecurities stockHistorizationSecurities) {
		entityManager.merge(stockHistorizationSecurities);
	}

	@Override
	public void deleteStockHistorizationSecurities(
			StockHistorizationSecurities stockHistorizationSecurities) {

		entityManager.remove(entityManager.merge(stockHistorizationSecurities));
	}

	@Override
	public StockHistorizationSecurities findStockHistorizationSecuritiesById(
			Integer idStockHistorizationSecurities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StockHistorizationSecurities> findAll() {
		Query query = entityManager.createQuery("select s from StockHistorizationSecurities s");
		return query.getResultList();
	}

}
