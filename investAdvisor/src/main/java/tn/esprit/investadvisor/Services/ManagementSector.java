package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Sector.IManagementSectorLocal;
import tn.esprit.investadvisor.contracts.Sector.IManagementSectorRemote;
import tn.esprit.investadvisor.entities.Company;
import tn.esprit.investadvisor.entities.Sector;

/**
 * Session Bean implementation class ManagementSector , it encapsulates all the
 * services provided by the EJB container for a {@link Sector} Entity.
 */
@Stateless
public class ManagementSector implements IManagementSectorLocal,
		IManagementSectorRemote {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	public ManagementSector() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link Sector} considered as new to the peristence
	 * context.When the entity manager comes to flush it synchronizes with the
	 * database
	 * 
	 * @author SEIF BOUANANI
	 * <p>
	 * 
	 */
	@Override
	public void createSector(Sector sector) {
		em.persist(sector);

	}

	/**
	 * This service adds a {@link Sector} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context.
	 */

	@Override
	public void updateSector(Sector sector) {
		em.merge(sector);
	}

	/**
	 * This service marks a {@link Sector} as being managed in the active
	 * peristence context as removed. If the entity is in context and labeled as
	 * "removed", it will be then removed form the database.
	 */
	@Override
	public void deleteSector(Sector sector) {
		em.remove(sector);
	}

	/**
	 * This service marks a {@link Sector} as being managed in the active
	 * peristence context as removed. If the entity is in context and labeled as
	 * "removed", it will remove form the database the Sector's Entity having
	 * the same id passed in paramater.
	 */
	@Override
	public void deleteSector(Integer id) {
		em.remove(id);
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Sector} by Id.
	 */
	@Override
	public Sector retrieveSector(Integer id) {
		return em.find(Sector.class, id);
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the This
	 * services uses JPQL to ask the entity manager to fetch all the
	 * {@link Sector}s from the database. from the database.
	 */
	@Override
	public List<Sector> retrieveAllSector() {
		Query query = em.createQuery("select c from Sector c", Sector.class);
		return query.getResultList();
	}

	@Override
	public Sector findSectorByLabel(String label) {
		TypedQuery<Sector> query = em.createQuery(
				"select a from Sector a where a.label =:sector",Sector.class);
		query.setParameter("sector", label);
		return query.getSingleResult();
	}

}
