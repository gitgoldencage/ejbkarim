package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.wallet.IManagementWalletLocal;
import tn.esprit.investadvisor.contracts.wallet.IManagementWalletRemote;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.Wallet;
import tn.esprit.investadvisor.entities.Wallet_Securities;
import tn.esprit.investadvisor.entities.Wallet_SecuritiesPK;

/**
 * Session Bean implementation class ManagementWallet , it implements all the
 * methods provided by the EJB container for {@link Wallet} Entity.
 * 
 * @author SEIF BOUANANI
 *         <p>
 */
@Stateless
@LocalBean
public class ManagementWallet implements IManagementWalletRemote,
		IManagementWalletLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementWallet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link Wallet} Entity considered as new to the
	 * peristence context.
	 */
	@Override
	public void addWallet(Wallet wallet) {

		entityManager.persist(wallet);

	}

	/**
	 * This service adds a {@link Wallet} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context.
	 */
	@Override
	public void updateWallet(Wallet wallet) {
		entityManager.merge(wallet);
	}

	/**
	 * This service marks a {@link Wallet} as being managed in the active
	 * peristence context as removed by passing a wallet entity in parameter.
	 */
	@Override
	public void deleteWallet(Wallet wallet) {

		entityManager.remove(entityManager.merge(wallet));
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Wallet} by Id .
	 */
	@Override
	public Wallet findWalletById(Integer idWallet) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link Wallet} from the database.
	 */
	@Override
	public List<Wallet> findAll() {
		Query query = entityManager.createQuery("select s from Wallet s");
		return query.getResultList();
	}

	@Override
	public void AssignSecuritiesToWallet(Wallet wallet, Securities securities,
			Integer quantity) {
		Wallet walletcurr = entityManager.find(Wallet.class, wallet.getId());
		Securities securitiescurr = entityManager.find(Securities.class,
				securities.getId());
		Wallet_Securities wallet_Securities = new Wallet_Securities();
		wallet_Securities.setWallet(walletcurr);
		wallet_Securities.setSecurities(securitiescurr);
		wallet_Securities.setQuantity(quantity);
		Wallet_SecuritiesPK pk = new Wallet_SecuritiesPK(walletcurr.getId(),
				securities.getId());
		wallet_Securities.setWallet_SecuritiesPK(pk);
		entityManager.merge(wallet_Securities);
	}

	@Override
	public List<Securities> findSecuritiesByWallet(Integer idWallet) {

		TypedQuery<Securities> query = entityManager
				.createQuery(
						"select s.securities from Wallet_Securities s where s.wallet.id =:wallet",
						Securities.class);
		query.setParameter("wallet", idWallet);
		return query.getResultList();
	}
	 @Override
	public List<Wallet_Securities> findAllSecurities_Wallet() {

		Query query = entityManager
				.createQuery("select s from Wallet_Securities s");
		return query.getResultList();
	}
	public void updateSECWALLET(Wallet_Securities ws){
		entityManager.merge(ws);
	}
	public void deleteSECWALLET(Wallet_Securities ws){
		entityManager.remove(entityManager.merge(ws));
	}
	public Wallet_Securities findSecWByid(Integer idWallet,Integer secc){
		TypedQuery<Wallet_Securities> query = entityManager
				.createQuery(
						"select s from Wallet_Securities s where s.wallet.id =:wallet And s.securities.id =:securitii",
						Wallet_Securities.class);
		query.setParameter("wallet", idWallet);
		query.setParameter("securitii", secc);
		return query.getSingleResult();
	}
}
