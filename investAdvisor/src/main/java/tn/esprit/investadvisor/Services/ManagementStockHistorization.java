package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.StockHistorization.IManagementStockHistorizationLocal;
import tn.esprit.investadvisor.contracts.StockHistorization.IManagementStockHistorizationRemote;
import tn.esprit.investadvisor.entities.Stock;
import tn.esprit.investadvisor.entities.StockHistorization;

/**
 * Session Bean implementation class ManagementStockHistorization , it
 * encapsulates all the services provided by the EJB container for a
 * {@link StockHistorization} entity.
 * 
 * @author SEIF BOUANANI
 *
 */
@Stateless
public class ManagementStockHistorization implements
		IManagementStockHistorizationRemote, IManagementStockHistorizationLocal {
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entitymanager;

	public ManagementStockHistorization() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void updateStockHistorization(StockHistorization stockH) {
		entitymanager.merge(stockH);
	}

	@Override
	public void deleteStockHistorization(StockHistorization stockH) {
		entitymanager.remove(entitymanager.merge(stockH));
	}

	@Override
	public List<StockHistorization> findAllStockHitorization() {
		Query query = entitymanager
				.createQuery("select st from StockHistorization st");
		return query.getResultList();
	}

	@Override
	public void addStockHistorization(StockHistorization stockH) {
		entitymanager.persist(stockH);

	}

	@Override
	public StockHistorization findStockHistorizationById(int id) {
		StockHistorization stockh = new StockHistorization();
		stockh = entitymanager.find(StockHistorization.class, id);
		return stockh;
	}

}
