package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyLocal;
import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyRemote;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Currency;

/**
 * <p>
 * Session Bean implementation class ManagementCurrency , it encapsulates all
 * the services provided by the EJB container for {@link Currency} Entity.
 * <p>
 * 
 * @author SEIF BOUANANI
 */
@Stateless
public class ManagementCurrency implements IManagementCurrencyLocal,
		IManagementCurrencyRemote {

	/**
	 * Default constructor.
	 */
	@PersistenceContext
	private EntityManager em;

	public ManagementCurrency() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds a <b>Bond</b> considered as new to the peristence
	 * context.When the entity manager comes to flush it synchronizes with the
	 * database. The entity manager rather stores it as managed or attached to
	 * the active persistence context. When the entity manager comes to flush it
	 * synchronizes with the database
	 * <p>
	 */
	@Override
	public void createCurrency(Currency currency) {
		em.persist(currency);
	}

	/**
	 * This service adds a {@link Currency} to the peristence context.The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context.
	 */
	@Override
	public void updateCurrency(Currency currency) {
		em.merge(currency);

	}

	/**
	 * This service marks a {@link Currency} as being managed in the active
	 * peristence context as <b>removed</b>.
	 */
	@Override
	public void deleteCurrency(Currency currency) {
		em.remove(em.merge(currency));

	}

	/**
	 * This service marks a {@link Currency} as being managed in the active
	 * peristence context as removed. When the entity manager comes to flush it
	 * synchronizes with thedatabase it will remove form the database ent
	 * {@link Currency} having the <b>Id</b> passed in parameter.
	 * <p>
	 */
	@Override
	public void deleteCurrency(Integer id) {
		em.remove(id);
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link Currency}by Id.
	 */
	@Override
	public Currency retrieveCurrency(Integer id) {
		return em.find(Currency.class, id);

	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * <b>Currencies</b> from the database.
	 */
	@Override
	public List<Currency> retrieveAllCurrency() {
		Query query = em
				.createQuery("select c from Currency c", Currency.class);
		// TypedQuery<Currency> query =
		// em.createQuery("select c from Currency c",
		// Currency.class);
		return query.getResultList();
	}

	@Override
	public Currency retrieveCurrencybyName(String name) {
		TypedQuery<Currency> query = em.createQuery(
				"select a from Currency a where a.name =:name",
				Currency.class);
		query.setParameter("name", name);
		return query.getSingleResult();
	}

}
