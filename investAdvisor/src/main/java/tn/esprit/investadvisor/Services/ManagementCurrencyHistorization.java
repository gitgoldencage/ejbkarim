package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.CurrencyHistorization.IManagementCurrencyHistorizationLocal;
import tn.esprit.investadvisor.contracts.CurrencyHistorization.IManagementCurrencyHistorizationRemote;
import tn.esprit.investadvisor.entities.CurrencyHistorization;

/**
 * Session Bean implementation class ManagementCurrencyHistorization , it
 * encapsulates all the services provided by the EJB container for a
 *  {@link CurrencyHistorization} Entity.
 *  <p>
 *  
 *  @author SEIF BOUANANI
 */
@Stateless
public class ManagementCurrencyHistorization implements
		IManagementCurrencyHistorizationRemote,
		IManagementCurrencyHistorizationLocal {

	/**
	 * Default constructor.
	 */

	@PersistenceContext
	private EntityManager em;

	public ManagementCurrencyHistorization() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link CurrencyHistorization} considered as new to the
	 * peristence context.
	 */
	@Override
	public void createCurrencyHistorization(
			CurrencyHistorization currencyHistorization) {
		em.persist(currencyHistorization);
	}

	/**
	 * This service adds a {@link CurrencyHistorization} to the peristence
	 * context.The entity manager rather stores it as managed or attached to the
	 * active persistence context.
	 */
	@Override
	public void updateCurrencyHistorization(
			CurrencyHistorization currencyHistorization) {
		em.merge(currencyHistorization);

	}

	/**
	 * This service marks a {@link CurrencyHistorization} as being managed in the
	 * active peristence context as removed. If the entity is in context and
	 * labeled as "removed", it will be then removed form the database.
	 */
	@Override
	public void deleteCurrencyHistorization(
			CurrencyHistorization currencyHistorization) {
		em.remove(currencyHistorization);

	}

	/**
	 * This service marks a {@link CurrencyHistorization} as being managed in the
	 * active peristence context as removed. When the entity manager comes to
	 * flush it synchronizes with thedatabase it will remove form the database
	 * ent {@link CurrencyHistorization} having the <b>Id</b> passed in
	 * parameter.
	 */
	@Override
	public void deleteCurrencyHistorization(Integer id) {
		em.remove(id);

	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a
	 * {@link CurrencyHistorization} by Id.
	 */
	@Override
	public CurrencyHistorization retrieveCurrencyHistorization(Integer id) {
		return em.find(CurrencyHistorization.class, id);

	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch all the
	 * {@link CurrencyHistorization} from the database.
	 */
	@Override
	public List<CurrencyHistorization> retrieveAllCurrencyHistorization() {
		Query query = em.createQuery("select c from CurrencyHistorization c",
				CurrencyHistorization.class);
		return query.getResultList();
	}

}
