package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.NonBankCompany.IManagementNonBankCompanyLocal;
import tn.esprit.investadvisor.contracts.NonBankCompany.IManagementNonBankCompanyRemote;
import tn.esprit.investadvisor.entities.NonBankCompany;

/**
 * Session Bean implementation class ManagementNonBankCompany , it encapsulates
 * all the services provided by the EJB container for a {@link NonBankCompany}
 * Entity.
 * 
 * @author SEIF BOUANANI
 */
@Stateless
@LocalBean
public class ManagementNonBankCompany implements
		IManagementNonBankCompanyRemote, IManagementNonBankCompanyLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementNonBankCompany() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * This service adds a {@link NonBankCompany} considered as new to the
	 * peristence context.When the entity manager comes to flush it synchronizes
	 * with the database
	 */
	@Override
	public void addNonBankCompany(NonBankCompany nonBankCompany) {

		entityManager.persist(nonBankCompany);

	}

	/**
	 * This service adds a {@link NonBankCompany} to the peristence context.The
	 * entity manager rather stores it as managed or attached to the active
	 * persistence context.
	 */
	@Override
	public void updateNonBankCompany(NonBankCompany nonBankCompany) {
		entityManager.merge(nonBankCompany);
	}

	/**
	 * This service marks a {@link NonBankCompany} being managed in the active
	 * peristence context as removed.
	 */
	@Override
	public void deleteNonBankCompany(NonBankCompany nonBankCompany) {

		entityManager.remove(entityManager.merge(nonBankCompany));
	}

	/**
	 * This service marks a {@link NonBankCompany} as being managed in the active
	 * peristence context as removed. When the entity manager comes to flush it
	 * synchronizes with thedatabase it will remove form the database ent
	 * NonBankCompany having the <b>Id</b> passed in parameter.
	 */
	@Override
	public NonBankCompany findNonBankCompanyById(Integer idNonBankCompany) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * . This services uses JPQL to ask the entity manager to fetch all the
	 * {@link NonBankCompany} from the database.
	 */
	@Override
	public List<NonBankCompany> findAll() {
		Query query = entityManager.createQuery("select s from NonBankCompany s");
		return query.getResultList();
	}
}
