package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.CentralBankCurrency.IManagementCentralBankCurrencyLocal;
import tn.esprit.investadvisor.contracts.CentralBankCurrency.IManagementCentralBankCurrencyRemote;
import tn.esprit.investadvisor.entities.CentralBankCurrency;

/**
 * Session Bean implementation class ManagementCentralBankCompany , it
 * encapsulates the services provided by the EJB container for a
 * {@link CentralBankCurrency} entity.
 * <p>
 */
@Stateless
@LocalBean
public class ManagementCentralBankCurrency implements
		IManagementCentralBankCurrencyRemote,
		IManagementCentralBankCurrencyLocal {

	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementCentralBankCurrency() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <p>
	 * This service adds a {@link CentralBankCurrency} considered as new to the
	 * peristence context.When the entity manager comes to flush it synchronizes
	 * with the database
	 * <p>
	 */
	@Override
	public void addCentralBankCurrency(CentralBankCurrency centralBankCurrency) {

		entityManager.persist(centralBankCurrency);

	}

	/**
	 * <p>
	 * This service adds a {@link CentralBankCurrency} to the peristence
	 * context.The entity manager rather stores it as managed or attached to the
	 * active persistence context. When the entity manager comes to flush it
	 * synchronizes with the database
	 * <p>
	 */
	@Override
	public void updateCentralBankCurrency(
			CentralBankCurrency centralBankCurrency) {
		entityManager.merge(centralBankCurrency);
	}

	/**
	 * <p>
	 * This service marks a {@link CentralBankCurrency} being managed in the
	 * active peristence context as removed. Thus, delete doesn't remove the
	 * Entity from thedatabase. When the entity manager comes to flush it
	 * synchronizes with thedatabase. If the entity is in context and labeled as
	 * "removed", it willbe then removed form the database.
	 * <p>
	 */
	@Override
	public void deleteCentralBankCurrency(
			CentralBankCurrency centralBankCurrency) {

		entityManager.remove(entityManager.merge(centralBankCurrency));
	}

	/**
	 * <p>
	 * This services uses JPQL to ask the entity manager to fetch a
	 * <b>BankCurrency</b> by Id.
	 * <p>
	 */
	@Override
	public CentralBankCurrency findCentralBankCurrencyById(
			Integer idCentralBankCurrency) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * <p>
	 * his services uses JPQL to ask the entity manager to fetch all the
	 * {@link CentralBankCurrency} from the database. As the reuslt fetching are more then one
	 * result , thus we're using "getResultList" and each result can be tuned
	 * via the object query.
	 * <p>
	 */
	@Override
	public List<CentralBankCurrency> findAll() {
		Query query = entityManager.createQuery("select s from Currency s");
		return query.getResultList();
	}
}
