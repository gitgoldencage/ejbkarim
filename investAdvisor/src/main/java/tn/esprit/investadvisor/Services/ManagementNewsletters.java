package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.investadvisor.contracts.Newsletters.ManagementNewslettersLocal;
import tn.esprit.investadvisor.contracts.Newsletters.ManagementNewslettersRemote;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Newsletters;

/**
 * Session Bean implementation class ManagementNewsletters
 */
@Stateless
public class ManagementNewsletters implements ManagementNewslettersRemote,
		ManagementNewslettersLocal {

	/**
	 * Default constructor.
	 */
	@PersistenceContext(unitName = "investAdvisor")
	EntityManager entityManager;

	public ManagementNewsletters() {
	}

	@Override
	public void addNewsletters(Newsletters newsletter) {
		entityManager.persist(newsletter);
	}

	@Override
	public void updateNewletters(Newsletters newsletter) {
		entityManager.merge(newsletter);
	}

	@Override
	public void deleteClaims(Newsletters newsletters) {
		
		entityManager.remove(entityManager.merge(newsletters));
	}

	@Override
	public Newsletters findNewsletterById(Integer idNewsletter) {
		Newsletters newsletters=new Newsletters();
		newsletters=entityManager.find(Newsletters.class, idNewsletter);
		return newsletters;
	}

	@Override
	public List<Newsletters> findAllNewsletters() {
		Query query = entityManager.createQuery("select n from Newsletters n");
		return query.getResultList();
	}

	@Override
	public Newsletters findNewsletterByName(String name) {
		TypedQuery<Newsletters> query = entityManager.createQuery(
				"select a from Newsletters a where a.name =:name",
				Newsletters.class);
		query.setParameter("name", name);
		return query.getSingleResult();
	}

}
