package tn.esprit.investadvisor.Services;

import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.investadvisor.contracts.Screeners.IManagementScreenersLocal;
import tn.esprit.investadvisor.contracts.Screeners.IManagementScreenersRemote;
import tn.esprit.investadvisor.entities.Screener;

/**
 * Session Bean implementation class ManagementScreeners
 */
@Stateless
@LocalBean
public class ManagementScreeners implements IManagementScreenersRemote, IManagementScreenersLocal {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
    public ManagementScreeners() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addScreener(Screener screener) {
		em.persist(screener);
		
	}

	@Override
	public void updateScreener(Screener screener) {
		em.merge(screener);
		
	}

	@Override
	public void deleteScreener(Screener screener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Screener findScreenerById(Integer idScreener) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Screener> findAllScreeners() {
		Query query = em.createQuery("select s from Screener s");
		return query.getResultList();
	}

}
