package tn.esprit.investadvisor.contracts.Company;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Company;
import tn.esprit.investadvisor.entities.Sector;
/**
 * the <b>IManagementCompanyLocal</b> interface provides the local access to the SessionBean all its
 * methods implemented in <b>ManagementCompanyLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementCompanyLocal {

	void addCompany(Company company);
	void updateCompany(Company company);

	void deleteCompany(Company company);
	Company findCompanyById(int id);
	List<Company> findCompanyBySector(Sector sector);
	Company findCompanyByName(String name);

	List<Company> findAllCompany();
}
