package tn.esprit.investadvisor.contracts.Stock;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Stock;
/**
 * the <b>IManagementStocksRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementStocksRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementStocksRemote {
	void addStock(Stock stock);

	void updateStock(Stock stock);

	void deleteStock(Stock stock);

	List<Stock> findAllStocks();

	Stock findStockById(int id);
}
