package tn.esprit.investadvisor.contracts.CurrencyHistorization;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.CurrencyHistorization;

@Remote
public interface IManagementCurrencyHistorizationRemote {
	public void createCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void updateCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void deleteCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void deleteCurrencyHistorization(Integer id);

	public CurrencyHistorization retrieveCurrencyHistorization(Integer id);

	public List<CurrencyHistorization> retrieveAllCurrencyHistorization();
}
