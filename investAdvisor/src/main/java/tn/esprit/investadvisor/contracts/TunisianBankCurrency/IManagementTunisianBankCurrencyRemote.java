package tn.esprit.investadvisor.contracts.TunisianBankCurrency;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.TunisanBankCurrency;
/**
 * the <b>IManagementTunisianBankCurrencyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementTunisianBankCurrencyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementTunisianBankCurrencyRemote {

	void addTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	void updateTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	void deleteTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	TunisanBankCurrency findTunisanBankCurrencyById(Integer idTunisanBankCurrency);

	List<TunisanBankCurrency> findAll();
}
