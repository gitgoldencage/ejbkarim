package tn.esprit.investadvisor.contracts.Claims;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Claims;

@Remote
public interface ManagementClaimsRemote {
	void addClaims(Claims claim);

	void updateClaims(Claims claims);

	void deleteClaims(Claims claims);

	Claims findClaimsById(Integer idClaims);

	List<Claims> findAllClaims();
	List<Claims> findclaimsByUserId(Integer userId);

	List<Claims> findClaimsByDate(Date date_claim);
}
