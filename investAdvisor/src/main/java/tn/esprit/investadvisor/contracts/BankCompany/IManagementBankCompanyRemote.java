package tn.esprit.investadvisor.contracts.BankCompany;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.BankCompany;
/**
 * the <b>IManagementBankCompanyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementBankCompanyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementBankCompanyRemote {

	void addBankCompany(BankCompany bonds);

	void updateBankCompany(BankCompany bonds);

	void deleteBankCompany(BankCompany bonds);

	BankCompany findBankCompanyById(Integer idBankCompany);

	List<BankCompany> findAll();
}
