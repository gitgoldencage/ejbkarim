package tn.esprit.investadvisor.contracts.wallet;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.Wallet;
import tn.esprit.investadvisor.entities.Wallet_Securities;
/**
 * the <b>IManagementWalletLocal</b> interface provides the local access to the SessionBean and all its
 * methods implemented in <b>MAanagementAdmin</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementWalletLocal {

	void addWallet(Wallet wallet);

	void updateWallet(Wallet wallet);

	void deleteWallet(Wallet wallet);

	Wallet findWalletById(Integer idWallet);

	List<Wallet> findAll();
	void AssignSecuritiesToWallet(Wallet wallet,Securities securities,Integer quantity);
	List<Securities> findSecuritiesByWallet(Integer idWallet);
	List<Wallet_Securities> findAllSecurities_Wallet();
	 void updateSECWALLET(Wallet_Securities ws);
	 void deleteSECWALLET(Wallet_Securities ws);
	 Wallet_Securities findSecWByid(Integer idWallet,Integer secc);
}
