package tn.esprit.investadvisor.contracts.Bonds;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Bonds;

/**
 * the <b>IManagementBondsLocal</b> interface provides the local access to the
 * SessionBean and all methods implemented in <b>ManagementBondsLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementBondsLocal {
	void addBonds(Bonds bonds);

	void updateBonds(Bonds bonds);

	void deleteBonds(Bonds bonds);

	Bonds findBondsById(Integer idBonds);

	List<Bonds> findAll();

}
