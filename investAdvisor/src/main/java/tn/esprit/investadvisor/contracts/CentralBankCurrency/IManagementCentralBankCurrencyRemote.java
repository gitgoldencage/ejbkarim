package tn.esprit.investadvisor.contracts.CentralBankCurrency;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.CentralBankCurrency;
/**
 * the <b>IManagementCentralBankCurrencyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementCentralBankCurrencyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementCentralBankCurrencyRemote {

	void addCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	void updateCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	void deleteCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	CentralBankCurrency findCentralBankCurrencyById(Integer idCentralBankCurrency);

	List<CentralBankCurrency> findAll();
}
