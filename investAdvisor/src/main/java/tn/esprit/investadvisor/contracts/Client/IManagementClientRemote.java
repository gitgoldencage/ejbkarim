package tn.esprit.investadvisor.contracts.Client;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Client;
/**
 * the <b>IManagementClientRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementClientRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementClientRemote {
	void addClient(Client client);

	void updateClient(Client client);

	void deleteClient(Client client);

	Client findClientById(int id);
	Client findClientByLogin(String login);
	boolean findClientByLoginBo(String login);

	List<Client> findClientByState(Integer state);
	List<Client> findClientByRegistrationDate(Date date);
	List<Client> findClientByLastConnectionDate(Date date);
	List<Client> findClientByLastConnectionDate();
	List<Client> findClientByLastConnectionDateNotNull();
	List<Client> findAllClient();
	Client findClientByEmail(String Email);
	boolean findClientByEmailBo(String Email);

}
