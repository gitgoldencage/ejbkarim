package tn.esprit.investadvisor.contracts.Operation;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Operation;

/**
 * the <b>IManagementOperationLocal</b> interface provides the local access to
 * the SessionBean and all its methods implemented in
 * <b>ManagementOperationLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementOperationLocal {

	void addOperation(Operation operation);

	void updateOperation(Operation operation);

	void deleteOperation(Operation operation);

	Operation findOperationById(Integer idOperation);

	List<Operation> findAll();
	List<Operation>findOperationByClient(Client client);
}
