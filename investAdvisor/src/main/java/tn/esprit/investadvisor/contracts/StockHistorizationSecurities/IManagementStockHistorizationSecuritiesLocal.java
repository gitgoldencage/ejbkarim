package tn.esprit.investadvisor.contracts.StockHistorizationSecurities;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.StockHistorizationSecurities;
/**
 * the <b>IManagementStockHistorizationSecuritiesLocal</b> interface provides the local access to the SessionBean all its 
 * methods implemented in <b>ManagementStockHistorizationSecuritiesLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementStockHistorizationSecuritiesLocal {

	void addStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	void updateStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	void deleteStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	StockHistorizationSecurities findStockHistorizationSecuritiesById(Integer idStockHistorizationSecurities);

	List<StockHistorizationSecurities> findAll();
}
