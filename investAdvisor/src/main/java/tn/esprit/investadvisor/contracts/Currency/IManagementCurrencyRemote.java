package tn.esprit.investadvisor.contracts.Currency;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Currency;
/**
 * the <b>IManagementCurrencyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementCurrencyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementCurrencyRemote {
	public void createCurrency(Currency currency);

	public void updateCurrency(Currency currency);

	public void deleteCurrency(Currency currency);

	public void deleteCurrency(Integer id);

	public Currency retrieveCurrency(Integer id);
	public Currency retrieveCurrencybyName(String name);
	

	public List<Currency> retrieveAllCurrency();
}
