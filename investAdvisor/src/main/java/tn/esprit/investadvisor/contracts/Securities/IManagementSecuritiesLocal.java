package tn.esprit.investadvisor.contracts.Securities;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Securities;

/**
 * the <b>ManagementSecuritiesLocal</b> interface provides the local access to
 * the SessionBean all its methods implemented in
 * <b>ManagementSecuritiesLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementSecuritiesLocal {

	void addSecurities(Securities securities);

	void updateSecurities(Securities securities);

	void deleteSecurities(Securities securities);

	Securities findSecuritiesById(Integer idSecurities);

	List<Securities> findAll();
}
