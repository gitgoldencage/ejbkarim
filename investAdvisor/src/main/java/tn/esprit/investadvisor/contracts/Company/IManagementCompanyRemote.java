package tn.esprit.investadvisor.contracts.Company;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Company;
import tn.esprit.investadvisor.entities.Sector;
/**
 * the <b>IManagementCompanyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementCompanyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementCompanyRemote {

	
	void addCompany(Company company);
	void updateCompany(Company company);

	void deleteCompany(Company company);
	Company findCompanyById(int id);
	List<Company> findCompanyBySector(Sector sector);
	Company findCompanyByName(String name);
	List<Company> findAllCompany();
}
