package tn.esprit.investadvisor.contracts.BankCompany;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.BankCompany;

/**
 * the <b>IManagementBankCompanyLocal</b> interface provides the local access to
 * the SessionBean all its methods implemented in
 * <b>ManagementBankCompanyLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementBankCompanyLocal {

	void addBankCompany(BankCompany bonds);

	void updateBankCompany(BankCompany bonds);

	void deleteBankCompany(BankCompany bonds);

	BankCompany findBankCompanyById(Integer idBankCompany);

	List<BankCompany> findAll();
}
