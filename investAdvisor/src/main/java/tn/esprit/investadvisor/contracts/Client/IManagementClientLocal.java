package tn.esprit.investadvisor.contracts.Client;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Client;
/**
 * the <b>IManagementClientLocal</b> interface provides the local access to the SessionBean and all its
 * methods implemented in <b>ManagementClientLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementClientLocal {
	void addClient(Client client);

	void updateClient(Client client);

	void deleteClient(Client client);

	Client findClientById(int id);
	Client findClientByLogin(String login);
	boolean findClientByLoginBo(String login);

	List<Client> findClientByState(Integer state);
	List<Client> findClientByRegistrationDate(Date date);
	List<Client> findClientByLastConnectionDate(Date date);
	List<Client> findClientByLastConnectionDate();
	List<Client> findClientByLastConnectionDateNotNull();
	List<Client> findAllClient();
	Client findClientByEmail(String Email);
	boolean findClientByEmailBo(String Email);

	
}
