package tn.esprit.investadvisor.contracts.Stock;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Stock;

/**
 * the <b>IManagementStocksLocal</b> interface provides the local access to the
 * SessionBean and all its methods implemented in <b>ManagementStocksLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementStocksLocal {

	void addStock(Stock stock);

	void updateStock(Stock stock);

	void deleteStock(Stock stock);

	List<Stock> findAllStocks();

	Stock findStockById(int id);

}
