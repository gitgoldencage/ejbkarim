package tn.esprit.investadvisor.contracts.Sector;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Sector;

/**
 * the <b>IManagementSectorLocal</b> interface provides the local access to
 * SessionBean and its methods implemented in <b>ManagementSectorLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementSectorLocal {
	public void createSector(Sector sector);

	public void updateSector(Sector sector);

	public void deleteSector(Sector sector);

	public void deleteSector(Integer id);
	public Sector findSectorByLabel(String label);
	public Sector retrieveSector(Integer id);

	public List<Sector> retrieveAllSector();
}
