package tn.esprit.investadvisor.contracts.NonBankCompany;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.NonBankCompany;
/**
 * the <b>IManagementNonBankCompanyRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>IManagementNonBankCompanyRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementNonBankCompanyRemote {

	void addNonBankCompany(NonBankCompany nonBankCompany);

	void updateNonBankCompany(NonBankCompany nonBankCompany);

	void deleteNonBankCompany(NonBankCompany nonBankCompany);

	NonBankCompany findNonBankCompanyById(Integer idNonBankCompany);

	List<NonBankCompany> findAll();
}
