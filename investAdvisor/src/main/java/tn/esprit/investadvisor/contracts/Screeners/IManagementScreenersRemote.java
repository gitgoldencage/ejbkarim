package tn.esprit.investadvisor.contracts.Screeners;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Claims;
import tn.esprit.investadvisor.entities.Screener;

@Remote
public interface IManagementScreenersRemote {

	void addScreener(Screener screener);

	void updateScreener(Screener screener);

	void deleteScreener(Screener screener);

	Screener findScreenerById(Integer idScreener);

	List<Screener> findAllScreeners();
	
	
}
