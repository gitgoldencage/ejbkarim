package tn.esprit.investadvisor.contracts.StockHistorization;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.StockHistorization;
/**
 * the <b>IManagementStockHistorizationLocal</b> interface provides the local access to the SessionBean and all its
 * methods implemented in <b>ManagementStockHistorizationLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementStockHistorizationLocal {
	void addStockHistorization(StockHistorization stockH);

	void updateStockHistorization(StockHistorization stockH);

	void deleteStockHistorization(StockHistorization stockH);
	
	StockHistorization findStockHistorizationById(int id);
	List<StockHistorization> findAllStockHitorization();
	

}
