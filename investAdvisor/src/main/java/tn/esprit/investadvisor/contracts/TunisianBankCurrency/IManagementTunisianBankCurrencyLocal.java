package tn.esprit.investadvisor.contracts.TunisianBankCurrency;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.TunisanBankCurrency;

/**
 * the <b>IManagementTunisianBankCurrencyLocal</b> interface provides the local
 * access to the SessionBean and all its methods implemented in
 * <b>ManagementTunisianBankCurrencyLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementTunisianBankCurrencyLocal {

	void addTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	void updateTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	void deleteTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency);

	TunisanBankCurrency findTunisanBankCurrencyById(
			Integer idTunisanBankCurrency);

	List<TunisanBankCurrency> findAll();
}
