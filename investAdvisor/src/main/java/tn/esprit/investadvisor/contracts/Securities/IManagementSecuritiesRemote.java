package tn.esprit.investadvisor.contracts.Securities;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Securities;
/**
 * the <b>IManagementSecuritiesRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementSecuritiesRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementSecuritiesRemote {

	void addSecurities(Securities securities);

	void updateSecurities(Securities securities);

	void deleteSecurities(Securities securities);

	Securities findSecuritiesById(Integer idSecurities);

	List<Securities> findAll();
}
