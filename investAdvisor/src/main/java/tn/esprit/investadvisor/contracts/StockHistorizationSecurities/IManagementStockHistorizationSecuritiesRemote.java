package tn.esprit.investadvisor.contracts.StockHistorizationSecurities;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.StockHistorizationSecurities;

@Remote
public interface IManagementStockHistorizationSecuritiesRemote {

	void addStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	void updateStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	void deleteStockHistorizationSecurities(StockHistorizationSecurities stockHistorizationSecurities);

	StockHistorizationSecurities findStockHistorizationSecuritiesById(Integer idStockHistorizationSecurities);

	List<StockHistorizationSecurities> findAll();
}
