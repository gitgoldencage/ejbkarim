package tn.esprit.investadvisor.contracts.Operation;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Operation;
/**
 * the <b>IManagementOperationRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementOperationRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementOperationRemote {

	void addOperation(Operation operation);

	void updateOperation(Operation operation);

	void deleteOperation(Operation operation);

	Operation findOperationById(Integer idOperation);

	List<Operation> findAll();
	List<Operation>findOperationByClient(Client client);
}
