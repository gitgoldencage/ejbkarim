package tn.esprit.investadvisor.contracts.CurrencyHistorization;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.CurrencyHistorization;

/**
 * the <b>IManagementCurrencyHistorizationLocal</b> interface provides the local
 * access to the SessionBean and all its methods implemented in
 * <b>ManagementCurrencyHistorizationLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementCurrencyHistorizationLocal {
	public void createCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void updateCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void deleteCurrencyHistorization(
			CurrencyHistorization currencyHistorization);

	public void deleteCurrencyHistorization(Integer id);

	public CurrencyHistorization retrieveCurrencyHistorization(Integer id);

	public List<CurrencyHistorization> retrieveAllCurrencyHistorization();
}
