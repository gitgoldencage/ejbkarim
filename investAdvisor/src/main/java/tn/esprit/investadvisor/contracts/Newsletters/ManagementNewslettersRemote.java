package tn.esprit.investadvisor.contracts.Newsletters;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Newsletters;

@Remote
public interface ManagementNewslettersRemote {
	void addNewsletters(Newsletters newsletter);

	void updateNewletters(Newsletters newsletter);

	void deleteClaims(Newsletters newsletters);

	Newsletters findNewsletterById(Integer idNewsletter);

	List<Newsletters> findAllNewsletters();
	Newsletters findNewsletterByName(String name);
}
