package tn.esprit.investadvisor.contracts.NonBankCompany;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.NonBankCompany;

/**
 * the <b>IManagementNonBankCompanyLocal</b> interface provides the local access
 * to the SessionBean and all its methods implemented in
 * <b>ManagementNonBankCompanyLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementNonBankCompanyLocal {

	void addNonBankCompany(NonBankCompany nonBankCompany);

	void updateNonBankCompany(NonBankCompany nonBankCompany);

	void deleteNonBankCompany(NonBankCompany nonBankCompany);

	NonBankCompany findNonBankCompanyById(Integer idNonBankCompany);

	List<NonBankCompany> findAll();
}
