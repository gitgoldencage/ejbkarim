package tn.esprit.investadvisor.contracts.wallet;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.Wallet;
import tn.esprit.investadvisor.entities.Wallet_Securities;
/**
 * the <b>IManagementWalletRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementWalletRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementWalletRemote {

	void addWallet(Wallet wallet);

	void updateWallet(Wallet wallet);

	void deleteWallet(Wallet wallet);

	Wallet findWalletById(Integer idWallet);

	List<Wallet> findAll();
	void AssignSecuritiesToWallet(Wallet wallet,Securities securities,Integer quantity);
	List<Securities> findSecuritiesByWallet(Integer idWallet);
	List<Wallet_Securities> findAllSecurities_Wallet();

}
