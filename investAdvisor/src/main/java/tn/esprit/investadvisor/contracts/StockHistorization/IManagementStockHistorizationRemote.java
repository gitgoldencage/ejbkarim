package tn.esprit.investadvisor.contracts.StockHistorization;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.StockHistorization;
/**
 * the <b>IManagementStockHistorizationRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementStockHistorizationRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementStockHistorizationRemote {
	void addStockHistorization(StockHistorization stockH);

	void updateStockHistorization(StockHistorization stockH);

	void deleteStockHistorization(StockHistorization stockH);

	StockHistorization findStockHistorizationById(int id);

	List<StockHistorization> findAllStockHitorization();

}
