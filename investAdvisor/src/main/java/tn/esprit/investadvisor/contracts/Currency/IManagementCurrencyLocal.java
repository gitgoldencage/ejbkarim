package tn.esprit.investadvisor.contracts.Currency;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Currency;

/**
 * the <b>IManagementAdminLocal</b> interface provides the local access to the
 * SessionBean and all its methods implemented in <b>MAanagementAdmin</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementCurrencyLocal {
	public void createCurrency(Currency currency);

	public void updateCurrency(Currency currency);

	public void deleteCurrency(Currency currency);

	public void deleteCurrency(Integer id);

	public Currency retrieveCurrency(Integer id);

	public List<Currency> retrieveAllCurrency();
}
