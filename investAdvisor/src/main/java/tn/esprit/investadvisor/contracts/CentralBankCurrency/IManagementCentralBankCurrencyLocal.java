package tn.esprit.investadvisor.contracts.CentralBankCurrency;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.CentralBankCurrency;
/**
 * the <b>IManagementCentralBankCurrencyLocal</b> interface provides the local access to the SessionBean and all
 * its methods implemented in <b>ManagementCentralBankCurrencyLocal</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementCentralBankCurrencyLocal {

	void addCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	void updateCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	void deleteCentralBankCurrency(CentralBankCurrency centralBankCurrency);

	CentralBankCurrency findCentralBankCurrencyById(
			Integer idCentralBankCurrency);

	List<CentralBankCurrency> findAll();
}
