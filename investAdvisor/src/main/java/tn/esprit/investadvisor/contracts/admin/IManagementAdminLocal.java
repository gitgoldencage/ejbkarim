package tn.esprit.investadvisor.contracts.admin;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Admin;

/**
 * the <b>IManagementAdminLocal</b> interface provides the local access to
 * SessionBean and all its methods implemented in <b>MAanagementAdmin</b>.
 * 
 * @author SEIF BOUANANI
 *
 */
@Local
public interface IManagementAdminLocal {

	void addAdmin(Admin admin);

	void updateAdmin(Admin admin);

	void deleteAdmin(Admin admin);

	Admin findAdminByLogin(String login);

	Admin findAdminById(Integer idAdmin);

	List<Admin> findAllAdmin();
}
