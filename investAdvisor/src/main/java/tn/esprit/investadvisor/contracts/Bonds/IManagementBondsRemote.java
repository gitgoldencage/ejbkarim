package tn.esprit.investadvisor.contracts.Bonds;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Bonds;
/**
 * the <b>IManagementBondsRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementBondsRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementBondsRemote {
	void addBonds(Bonds bonds);

	void updateBonds(Bonds bonds);

	void deleteBonds(Bonds bonds);

	Bonds findBondsById(Integer idBonds);

	List<Bonds> findAll();
}
