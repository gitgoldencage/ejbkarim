package tn.esprit.investadvisor.contracts.admin;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Admin;

/**
 * the <b>IManagementAdminRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementAdmin</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementAdminRemote {
	void addAdmin(Admin admin);

	void updateAdmin(Admin admin);

	void deleteAdmin(Admin admin);

	Admin findAdminByLogin(String login);

	Admin findAdminById(Integer idAdmin);

	List<Admin> findAllAdmin();
}
