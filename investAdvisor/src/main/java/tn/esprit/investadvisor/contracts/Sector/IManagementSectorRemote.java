package tn.esprit.investadvisor.contracts.Sector;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.investadvisor.entities.Sector;
/**
 * the <b>IManagementSectorRemote</b> interface provides a remote access to the
 * bean and its services implemeted in <b>ManagementSectorRemote</b> .
 * 
 * 
 * @author SEIF
 *
 */
@Remote
public interface IManagementSectorRemote {
	public void createSector(Sector sector);

	public void updateSector(Sector sector);

	public void deleteSector(Sector sector);

	public void deleteSector(Integer id);

	public Sector retrieveSector(Integer id);
	
	public Sector findSectorByLabel(String label);
	public List<Sector> retrieveAllSector();
}
