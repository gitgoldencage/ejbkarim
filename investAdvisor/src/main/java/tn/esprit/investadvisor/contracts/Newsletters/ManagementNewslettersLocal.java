package tn.esprit.investadvisor.contracts.Newsletters;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Newsletters;


@Local
public interface ManagementNewslettersLocal {
	void addNewsletters(Newsletters newsletter);

	void updateNewletters(Newsletters newsletter);

	void deleteClaims(Newsletters newsletters);

	Newsletters findNewsletterById(Integer idNewsletter);

	Newsletters findNewsletterByName(String name);
	List<Newsletters> findAllNewsletters();
}
