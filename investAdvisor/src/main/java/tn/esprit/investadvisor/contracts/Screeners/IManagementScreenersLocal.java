package tn.esprit.investadvisor.contracts.Screeners;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.investadvisor.entities.Claims;
import tn.esprit.investadvisor.entities.Screener;

@Local
public interface IManagementScreenersLocal {

	void addScreener(Screener screener);

	void updateScreener(Screener screener);

	void deleteScreener(Screener screener);

	Screener findScreenerById(Integer idScreener);

	List<Screener> findAllScreeners();
}
