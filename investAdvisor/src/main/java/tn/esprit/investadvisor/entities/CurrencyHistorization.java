package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CurencyHistorization
 *<p>
 * This {@link CurrencyHistorization} is the Entity responsible for storing the
 * fluctuation of the TND agains other currencies . Values persisted the Database will be
 * encapsulated to build the screeners that describes the variations.
 * 
 * @author SEIF BOUANANI
 */
@Entity
@Table(name = "t_currencyHistorization")
public class CurrencyHistorization implements Serializable {

	private Integer id;
	private Date date;
	private static final long serialVersionUID = 1L;
	private CentralBankCurrency centralBankCurrency;

	public CurrencyHistorization() {
		super();
	}

	@Id
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@ManyToOne
	@JoinColumn(name = "central_bank_currency")
	public CentralBankCurrency getCentralBankCurrency() {
		return centralBankCurrency;
	}

	public void setCentralBankCurrency(CentralBankCurrency centralBankCurrency) {
		this.centralBankCurrency = centralBankCurrency;
	}

}
