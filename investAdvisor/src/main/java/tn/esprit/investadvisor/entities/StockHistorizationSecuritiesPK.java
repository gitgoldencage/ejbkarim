package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class StockHistorizationSecuritiesPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer stockHistorizationId;
	private Integer securitiesId;
	private Date date;
	
	
	
	
	
	public StockHistorizationSecuritiesPK() {
		super();
	}
	
	
	@Column(name="stock_historization_id")
	public Integer getStockHistorizationId() {
		return stockHistorizationId;
	}
	public void setStockHistorizationId(Integer stockHistorizationId) {
		this.stockHistorizationId = stockHistorizationId;
	}
	@Column(name="securities_id")
	public Integer getSecuritiesId() {
		return securitiesId;
	}
	public void setSecuritiesId(Integer securitiesId) {
		this.securitiesId = securitiesId;
	}
	
	
	
	

	public StockHistorizationSecuritiesPK(Integer stockHistorizationId,
			Integer securitiesId, Date date) {
		super();
		this.stockHistorizationId = stockHistorizationId;
		this.securitiesId = securitiesId;
		this.date = date;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((securitiesId == null) ? 0 : securitiesId.hashCode());
		result = prime
				* result
				+ ((stockHistorizationId == null) ? 0 : stockHistorizationId
						.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockHistorizationSecuritiesPK other = (StockHistorizationSecuritiesPK) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (securitiesId == null) {
			if (other.securitiesId != null)
				return false;
		} else if (!securitiesId.equals(other.securitiesId))
			return false;
		if (stockHistorizationId == null) {
			if (other.stockHistorizationId != null)
				return false;
		} else if (!stockHistorizationId.equals(other.stockHistorizationId))
			return false;
		return true;
	}


	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
}
