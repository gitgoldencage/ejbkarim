package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.util.Comparator;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Wallet
 * <p>
 * The Wallet is the Entity that provides to {@link Client}s the opprtunity to
 * perform their decisions of purchasing or Introduce in sell for securities.
 * Each Approved {@link Client} has is own {@link Wallet}, in which we can
 * distinguish the Amount of account , his {@link Operation}s and
 * {@link Securities} bought and susceptible to be introduced in Sell.
 * 
 *
 */
@Entity
@Table(name = "t_wallet")
public class Wallet implements Serializable,Comparable<Wallet> {

	private Integer id;
	private Float walletValue;
	private Float riskfree;
	private Float securitiesValue;
	public Float getRiskfree() {
		return riskfree;
	}

	public void setRiskfree(Float riskfree) {
		this.riskfree = riskfree;
	}

	public Float getSecuritiesValue() {
		return securitiesValue;
	}

	public void setSecuritiesValue(Float securitiesValue) {
		this.securitiesValue = securitiesValue;
	}



	private static final long serialVersionUID = 1L;
	private Client client;
	private List<Operation> operations;
	private List<Wallet_Securities> wallet_Securities;

	public Wallet() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getWalletValue() {
		return this.walletValue;
	}

	public void setWalletValue(Float walletValue) {
		this.walletValue = walletValue;
	}

	/*
	 * public Integer getValid() { return this.valid; }
	 * 
	 * public void setValid(Integer valid) { this.valid = valid; }
	 */

	@OneToOne
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@OneToMany(mappedBy = "wallet" ,fetch=FetchType.EAGER)
	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	

	
	@OneToMany(mappedBy = "wallet")
	public List<Wallet_Securities> getWallet_Securities() {
		return wallet_Securities;
	}

	public void setWallet_Securities(List<Wallet_Securities> wallet_Securities) {
		this.wallet_Securities = wallet_Securities;
	}

	@Override
	public int compareTo(Wallet o) {
		// TODO Auto-generated method stub
		return walletValue.compareTo(o.walletValue);
	}

}
