package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Embeddable
public class OperationPk implements Serializable{

	/**
	 * 
	 */
	private Integer walletId;
	private Integer securitiesId;
	private Date operationDate;
	@Column(name="wallet_id")
	public Integer getWalletId() {
		return walletId;
	}
	public void setWalletId(Integer walletId) {
		this.walletId = walletId;
	}
	@Column(name="securities_id")
	public Integer getSecuritiesId() {
		return securitiesId;
	}
	public void setSecuritiesId(Integer securitiesId) {
		this.securitiesId = securitiesId;
	}
	private static final long serialVersionUID = 1L;

	public OperationPk(Integer walletId, Integer securitiesId,Date operationDate) {
		super();
		this.walletId = walletId;
		this.securitiesId = securitiesId;
		this.operationDate=operationDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((operationDate == null) ? 0 : operationDate.hashCode());
		result = prime * result
				+ ((securitiesId == null) ? 0 : securitiesId.hashCode());
		result = prime * result
				+ ((walletId == null) ? 0 : walletId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationPk other = (OperationPk) obj;
		if (operationDate == null) {
			if (other.operationDate != null)
				return false;
		} else if (!operationDate.equals(other.operationDate))
			return false;
		if (securitiesId == null) {
			if (other.securitiesId != null)
				return false;
		} else if (!securitiesId.equals(other.securitiesId))
			return false;
		if (walletId == null) {
			if (other.walletId != null)
				return false;
		} else if (!walletId.equals(other.walletId))
			return false;
		return true;
	}
	public OperationPk() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
}
