package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Entity implementation class for Entity: NonBankCompany
 * <p>
 * This Entity is a subclass of {@link Company} . it inherite all the attributes
 * and methods from the superclass.
 * 
 * The Non Banks Companies represents all the companies quoted in the Stock
 * Market other than The Banks.
 * 
 * 
 * @author SEIF BOUANANI
 */
@Entity
@DiscriminatorValue("Non Bank Company")
public class NonBankCompany extends Company implements Serializable {

	private static final long serialVersionUID = 1L;

	public NonBankCompany() {
		super();
	}

}
