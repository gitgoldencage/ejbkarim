package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="t_wallet_Securities")
public class Wallet_Securities implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer quantity;
	private Wallet_SecuritiesPK wallet_SecuritiesPK;
	private Securities securities;
	private Wallet wallet;
	
	
	@EmbeddedId
	public Wallet_SecuritiesPK getWallet_SecuritiesPK() {
		return wallet_SecuritiesPK;
	}

	public void setWallet_SecuritiesPK(Wallet_SecuritiesPK wallet_SecuritiesPK) {
		this.wallet_SecuritiesPK = wallet_SecuritiesPK;
	}

	@ManyToOne
	@JoinColumn(name="securities_id",referencedColumnName="id",insertable=false,updatable=false)
	public Securities getSecurities() {
		return securities;
	}

	public void setSecurities(Securities securities) {
		this.securities = securities;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="wallet_id",referencedColumnName="id",insertable=false,updatable=false)
	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public Wallet_Securities() {
		super();
	}
	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Wallet_Securities(Integer quantity, Securities securities,
			Wallet wallet) {
		super();
		this.quantity = quantity;
		this.wallet_SecuritiesPK = new Wallet_SecuritiesPK(wallet.getId(),securities.getId());
		this.securities = securities;
		this.wallet = wallet;
	}
	
	
	

	
}
