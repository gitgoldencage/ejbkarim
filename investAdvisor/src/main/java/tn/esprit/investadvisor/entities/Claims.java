package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Claims
 *
 */
@Entity
@Table(name="t_claims")
public class Claims implements Serializable {

	
	private Integer id;
	private String message;
	private Integer userFrom;
	private Client client;
	private Date date_claim;
	private String textClaims;
	private static final long serialVersionUID = 1L;

	public Claims() {
		
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}   
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}   
	public Integer getUserFrom() {
		return this.userFrom;
	}

	public void setUserFrom(Integer userFrom) {
		this.userFrom = userFrom;
	}
	@ManyToOne
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getDate_claim() {
		return date_claim;
	}
	public void setDate_claim(Date date_claim) {
		this.date_claim = date_claim;
	}
	@Column(length=3000)
	public String getTextClaims() {
		return textClaims;
	}
	public void setTextClaims(String textClaims) {
		this.textClaims = textClaims;
	}
   
}
