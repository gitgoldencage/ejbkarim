package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import tn.esprit.investadvisor.entities.Company;

/**
 * Entity implementation class for Entity: BankCampany
 * <p>
 * This Entity represents the type of Bank companies which extend from the type
 * of {@link Company}. Each {@link BankCompany} provides its {@link Currency}
 * pairs and its rate exchanges which will help custumers to have a global view
 * on the Forex Market and local curency exchange.
 *<p>
 * @author SEIF BOUANANI
 */
@Entity
@DiscriminatorValue("Bank Company")
public class BankCompany extends Company implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<TunisanBankCurrency> tunisianBankCurrencies;

	public BankCompany() {
		super();
	}

	@OneToMany(mappedBy = "bankCompany")
	public List<TunisanBankCurrency> getTunisianBankCurrencies() {
		return tunisianBankCurrencies;
	}

	public void setTunisianBankCurrencies(
			List<TunisanBankCurrency> tunisianBankCurrencies) {
		this.tunisianBankCurrencies = tunisianBankCurrencies;
	}

}
