package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import javax.persistence.*;
import tn.esprit.investadvisor.entities.Securities;

/**
 * Entity implementation class for Entity: Bonds
 * <p>
 * Technicaly {@link Bonds} is subclass derived from {@link Securities} Entity .
 * 
 * {@link Bonds} represents one of the essential security type to handle in our
 * application business purposes .
 * <p>
 * In Finance , {@link Bonds} is a debt investment in which an investor loans money
 * to an entity (corporate or governmental) that borrows the funds for a defined
 * period of time at a fixed interest rate. 
 * <p>
 * @author SEIF BOUANANI
 *
 */
@Entity
@DiscriminatorValue(value = "bonds")
public class Bonds extends Securities implements Serializable {

	private static final long serialVersionUID = 1L;

	public Bonds() {
		super();
	}

}
