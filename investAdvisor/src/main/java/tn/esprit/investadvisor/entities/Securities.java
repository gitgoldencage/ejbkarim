package tn.esprit.investadvisor.entities;

import java.util.List;
import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Securities
 *<p>
 * This Entity is a superclass managed by an<b>"SINGLE_TABLE"</b> Inheritance
 * Type . the {@link DiscriminatorColumn} for its subclasses {@link Bonds} and
 * {@link Stock}s is the <b>"Security_type"</b>.
 * 
 * It contains all the attributes of a Securities such as theirs prices and
 * session date .
 * <p>
 * As a fincancail terms , Securities are Financing or investment instruments
 * suceptible to be bought and sold in financial markets.
 * <p>
 * However, {@link Securities} is the most important part of the business docket
 * of the applications.
 * 
 * @author SEIF BOUANANI
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "security_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "t_securities")
public abstract class Securities implements Serializable {

	private Integer id;
	private String value;
	private Date session;
	private Float capital;
	private Float lastPrice;
	private Float closingPrice;
	private Float variabiliy;
	private Integer quantity;
	private Float highest;
	private Float lowest;
	private Integer tradedQuantity;
	private List<Wallet_Securities> wallet_Securities;
	private static final long serialVersionUID = 1L;

	private List<Currency> currencies;
	private List<StockHistorizationSecurities> stockHistorizationSecurities;
	private List<Operation> operations;
	

	public Securities() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getSession() {
		return this.session;
	}

	public void setSession(Date session) {
		this.session = session;
	}

	@Column(name = "closing_price")
	public Float getClosingPrice() {
		return this.closingPrice;
	}

	public void setClosingPrice(Float closingPrice) {
		this.closingPrice = closingPrice;
	}

	public Float getVariabiliy() {
		return this.variabiliy;
	}

	public void setVariabiliy(Float variabiliy) {
		this.variabiliy = variabiliy;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@OneToMany(mappedBy = "securities")
	public List<Currency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<Currency> currencies) {
		this.currencies = currencies;
	}

	public Float getCapital() {
		return capital;
	}

	public void setCapital(Float capital) {
		this.capital = capital;
	}

	public Float getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(Float lastPrice) {
		this.lastPrice = lastPrice;
	}

	public Float getHighest() {
		return highest;
	}

	public void setHighest(Float highest) {
		this.highest = highest;
	}

	public Float getLowest() {
		return lowest;
	}

	public void setLowest(Float lowest) {
		this.lowest = lowest;
	}

	public Integer getTradedQuantity() {
		return tradedQuantity;
	}

	public void setTradedQuantity(Integer tradedQuantity) {
		this.tradedQuantity = tradedQuantity;
	}

	@OneToMany(mappedBy = "securities")
	public List<StockHistorizationSecurities> getStockHistorizationSecurities() {
		return stockHistorizationSecurities;
	}

	public void setStockHistorizationSecurities(
			List<StockHistorizationSecurities> stockHistorizationSecurities) {
		this.stockHistorizationSecurities = stockHistorizationSecurities;
	}

	@OneToMany(mappedBy = "securities")
	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	@OneToMany(mappedBy = "securities")
	public List<Wallet_Securities> getWallet_Securities() {
		return wallet_Securities;
	}

	public void setWallet_Securities(List<Wallet_Securities> wallet_Securities) {
		this.wallet_Securities = wallet_Securities;
	}

	

}
