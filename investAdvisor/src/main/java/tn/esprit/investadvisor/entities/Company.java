package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Company
 * <p>
 * This Entity is a superclass managed by an<b>"SINGLE_TABLE"</b> Inheritance
 * Type . the {@link DiscriminatorColumn} for its subclasses {@link BankCompany}
 * and {@link NonBankCompany} is the <b>"Bank_type"</b>.
 * <p>
 * In a Finance context and more specifically in Stock Markets , each
 * {@link Company} has its {@link Securities} offers.
 *
 *@author SEIF BOUANANI
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "bank_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "t_company")
public abstract class Company implements Serializable {

	protected Integer id;
	protected String name;
	protected String phone;
	private String contact;
	protected String address;
	protected String leadership;
	protected String Shares; 
	protected String floating;
	protected String TotalCapital;
	protected String description;
	protected static final long serialVersionUID = 1L;
	//relations//
	
	private Sector sector;
	
	//private Securities securitie;

	public Company() {}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getLeadership() {
		return leadership;
	}

	public void setLeadership(String leadership) {
		this.leadership = leadership;
	}

	public String getShares() {
		return Shares;
	}

	public void setShares(String shares) {
		Shares = shares;
	}

	public String getTotalCapital() {
		return TotalCapital;
	}

	public void setTotalCapital(String totalCapital) {
		TotalCapital = totalCapital;
	}
	
	@Column(columnDefinition="TEXT")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(columnDefinition="TEXT")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@ManyToOne
	@JoinColumn(name = "sector")
	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	
	

	

	public String getFloating() {
		return floating;
	}

	public void setFloating(String floating) {
		this.floating = floating;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

}
