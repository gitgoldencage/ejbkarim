
package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Curency
 *<p>
 * This Entity is a superclass , has <b>"SINGLE_TYPE"</b> as a
 * {@link Inheritance} type . Its subclasses will be discriminated in a
 * {@link DiscriminatorColumn}="<b>currency_source</b>" .
 * 
 * @author SEIF BOUANANI
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "currency_source", discriminatorType = DiscriminatorType.STRING)
@Table(name = "t_currency")
public class Currency implements Serializable {

	private Integer id;
	private Float offer;
	private String name;
	private String ind;
	private String invoffer;
	private String logo;
	private static final long serialVersionUID = 1L;
	private Securities securities;

	public Currency() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getOffer() {
		return this.offer;
	}

	public void setOffer(Float offer) {
		this.offer = offer;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@ManyToOne
	@JoinColumn(name = "securities")
	public Securities getSecurities() {
		return securities;
	}

	public void setSecurities(Securities securities) {
		this.securities = securities;
	}

	public String getInd() {
		return ind;
	}

	public void setInd(String ind) {
		this.ind = ind;
	}

	public String getInvoffer() {
		return invoffer;
	}

	public void setInvoffer(String invoffer) {
		this.invoffer = invoffer;
	}

}
