package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import tn.esprit.investadvisor.entities.Currency;

/**
 * Entity implementation class for Entity: CentralBankCurrency
 * <p>
 * The {@link CentralBankCurrency} are a type of {@link Currency}.
 * <p>
 * The Central Bank Currencies will be basically used as a main currency
 * reference in The {@link Operation}s such as Purchasing and selling securities By {@link Client}s
 * in their fictif {@link Wallet}'s simulation .
 *
 * @author SEIF BOUANANI
 */
@Entity
@DiscriminatorValue("Central Bank Currency")
public class CentralBankCurrency extends Currency implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<CurrencyHistorization> currencyHistorizations;

	@OneToMany(mappedBy = "centralBankCurrency")
	public List<CurrencyHistorization> getCurrencyHistorizations() {
		return currencyHistorizations;
	}

	public void setCurrencyHistorizations(
			List<CurrencyHistorization> currencyHistorizations) {
		this.currencyHistorizations = currencyHistorizations;
	}

	public CentralBankCurrency() {
		super();
	}

}
