package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="t_StockHistorization_Securities")
public class StockHistorizationSecurities implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Float value;
	
	private StockHistorizationSecuritiesPK stockHistorizationSecuritiesPK;
	private Securities securities;
	private StockHistorization stockHistorization;
	
	public StockHistorizationSecurities() {
		super();
	}
	
	public Float getValue() {
		return value;
	}
	public void setValue(Float value) {
		this.value = value;
	}
	
	@EmbeddedId
	public StockHistorizationSecuritiesPK getStockHistorizationSecuritiesPK() {
		return stockHistorizationSecuritiesPK;
	}
	public void setStockHistorizationSecuritiesPK(
			StockHistorizationSecuritiesPK stockHistorizationSecuritiesPK) {
		this.stockHistorizationSecuritiesPK = stockHistorizationSecuritiesPK;
	}	
	
	@ManyToOne
	@JoinColumn(name="securities_id",referencedColumnName="id",insertable=false,updatable=false)
	public Securities getSecurities() {
		return securities;
	}
	public void setSecurities(Securities securities) {
		this.securities = securities;
	}
	@ManyToOne
	@JoinColumn(name="stock_historization_id",referencedColumnName="id",insertable=false,updatable=false)
	public StockHistorization getStockHistorization() {
		return stockHistorization;
	}
	public void setStockHistorization(StockHistorization stockHistorization) {
		this.stockHistorization = stockHistorization;
	}
	public StockHistorizationSecurities(Float value, Date date,
			Securities securities, StockHistorization stockHistorization) {
		super();
		this.value = value;
		
		this.securities = securities;
		this.stockHistorization = stockHistorization;
		this.setStockHistorizationSecuritiesPK(new StockHistorizationSecuritiesPK(stockHistorization.getId(),securities.getId(),date));
	}
	
}
