package tn.esprit.investadvisor.entities;

/**
 * <p>
 * State is an enumaration class that encapsulates the State of the
 * {@link Securities} wich are : Onsell,Sold or Bought.
 * 
 * @author SEIF BOUANANI
 *
 */
public enum State {
	OnSell, Sold, Bought

}
