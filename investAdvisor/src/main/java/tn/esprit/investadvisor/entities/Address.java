package tn.esprit.investadvisor.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
/**
 * 
 * 
 * @author SEIF BOUANANI
 *
 */
@Embeddable
public class Address implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private String country;
	private String town;
	private String street;
	
	public Address() {
			super();// TODO Auto-generated constructor stub
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	@Column(name="post_code")
	public Integer getPostCode() {
		return postCode;
	}
	public void setPostCode(Integer postCode) {
		this.postCode = postCode;
	}
	private Integer postCode;
}
