package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import tn.esprit.investadvisor.entities.Securities;

/**
 * Entity implementation class for Entity: Stock
 * <p>
 * this entity is subclass that inherit all its attributes and methods from the
 * superclass {@link Securities} .
 * 
 * Stocks represents one of the essential security type to handle in our
 * application business purposes .
 * <p>
 * 
 * In finance , Stocks are Equities capital raised through sale of shares. It
 * belongs often to {@link Company}'s value quoted in the Stock Market.
 * 
 * @author SEIF BOUANANI
 *
 */
@Entity
@DiscriminatorValue(value = "Stock")
public class Stock extends Securities implements Serializable {

	
	
private List<StockHistorization> stockhist;

	
	@OneToMany(mappedBy="stock")
	public List<StockHistorization> getStockhist() {
		return stockhist;
	}

	public void setStockhist(List<StockHistorization> stockhist) {
		this.stockhist = stockhist;
	}
	private static final long serialVersionUID = 1L;

	public Stock() {
		super();
	}

}
