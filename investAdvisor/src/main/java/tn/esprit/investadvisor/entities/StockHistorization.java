package tn.esprit.investadvisor.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: StockHistorization
 *<p>
 * The StockHistorization is the Entity responsible for storing the fluctuation
 * of securities . the Values persisted in the Database will be encapsulated to build
 * the screeners that describes the variations. 
 * 
 * @author SEIF BOUANANI
 * 
 */
@Entity
@Table(name = "t_stockHistorization")
public class StockHistorization implements Serializable {

	private Integer id;
	private String date;
	private String ouverture;
	private String dernier;
	private String plusHaut;
	private String plusBas;
	private String nbTransaction;
	private  String quantite;
	private String capitaux;
	private Stock stock;
	


	public String getOuverture() {
		return ouverture;
	}

	public void setOuverture(String ouverture) {
		this.ouverture = ouverture;
	}

	public String getDernier() {
		return dernier;
	}

	public void setDernier(String dernier) {
		this.dernier = dernier;
	}

	public String getPlusHaut() {
		return plusHaut;
	}

	public void setPlusHaut(String plusHaut) {
		this.plusHaut = plusHaut;
	}

	public String getPlusBas() {
		return plusBas;
	}

	public void setPlusBas(String plusBas) {
		this.plusBas = plusBas;
	}

	public String getNbTransaction() {
		return nbTransaction;
	}

	public void setNbTransaction(String nbTransaction) {
		this.nbTransaction = nbTransaction;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	public String getCapitaux() {
		return capitaux;
	}

	public void setCapitaux(String capitaux) {
		this.capitaux = capitaux;
	}

	private static final long serialVersionUID = 1L;
	private List<StockHistorizationSecurities> stockHistorizationSecurities;

	public StockHistorization() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	@OneToMany(mappedBy = "stockHistorization")
	public List<StockHistorizationSecurities> getStockHistorizationSecurities() {
		return stockHistorizationSecurities;
	}

	public void setStockHistorizationSecurities(
			List<StockHistorizationSecurities> stockHistorizationSecurities) {
		this.stockHistorizationSecurities = stockHistorizationSecurities;
	}

	
	
	@ManyToOne
	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
